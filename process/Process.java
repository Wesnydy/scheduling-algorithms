package process;

public class Process {
	private int priority;
	private int arrivalTime;
	private int executionTime;
	private int returnTime;
	private int responseTime;
	private int waitTime;

	public Process(int arrival, int execution, int prior) {
		arrivalTime = arrival;
		executionTime = execution;
		priority = prior;
		returnTime = 0;
		responseTime = 0;
		waitTime = 0;
	}
	
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int prior) {
		priority = prior;
	}
  
	public int getArrivalTime() {
		return arrivalTime;
	}
	
	public void setArrivalTime(int arrivaltime) {
		arrivalTime = arrivaltime;
	}
  
	public int getExecutionTime() {
		return executionTime;
	}
	
	public void setExecutionTime(int executiontime) {
		executionTime = executiontime;
	}

	public int getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(int returntime) {
		returnTime = returntime;
	}

	public int getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(int responsetime) {
		responseTime = responsetime;
	}

	public int getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(int waittime) {
		waitTime = waittime;
	}
}