package algorithms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import process.Process;

public class Algorithms {
	
	public static void fcfs(ArrayList<Process> processList) {
		int waitTime = 0;
		// First process do not wait
		processList.get(0).setWaitTime(waitTime);
		for (int i = 1; i < processList.size(); i++) {
			waitTime += processList.get(i-1).getExecutionTime();			
			processList.get(i).setWaitTime(waitTime - processList.get(i).getArrivalTime());
		}
		
		int returnTime;
		for (int i = 0; i < processList.size(); i++) {	
			returnTime = processList.get(i).getWaitTime() 
				+ processList.get(i).getExecutionTime();
			processList.get(i).setReturnTime(returnTime);
		}
		
		int responseTime;
		for (int i = 0; i < processList.size(); i++) {	
			responseTime = processList.get(i).getReturnTime()
				- processList.get(i).getExecutionTime();
			processList.get(i).setResponseTime(responseTime);
		}
		
		// Calculate average of return time, response time and wait time
		showAverageValues(processList);
		
	}
	
	public static void sjf(ArrayList<Process> processList) {
		for (int i = 0; i < processList.size(); i++) {
			for (int j = 0; j < processList.size() -1; j++) {
				if (processList.get(j).getPriority() > processList.get(j + 1).getPriority()) {
					Collections.swap(processList, j, j + 1);
			    }
			}
		}
		
		int waitTime = 0;
		// First process do not wait
		processList.get(0).setWaitTime(waitTime);
		for (int i = 1; i < processList.size(); i++) {
			waitTime += processList.get(i-1).getExecutionTime();			
			processList.get(i).setWaitTime(waitTime - processList.get(i).getArrivalTime());
		}
		
		int returnTime;
		for (int i = 0; i < processList.size(); i++) {	
			returnTime = processList.get(i).getWaitTime() 
				+ processList.get(i).getExecutionTime();
			processList.get(i).setReturnTime(returnTime);
		}
		
		int responseTime;
		for (int i = 0; i < processList.size(); i++) {	
			responseTime = processList.get(i).getReturnTime()
				- processList.get(i).getExecutionTime();
			processList.get(i).setResponseTime(responseTime);
		}
		
		// Calculate average of return time, response time and wait time
		showAverageValues(processList);
	}
	
	public static void rr(ArrayList<Process> processList, int quantum) {
		boolean flag = false;
		int count, time, remain;
		int waitTime = 0;
		int returnTime = 0;
		int responseTime = 0;
		
		int listSize = processList.size();
		int rt[] = new int[listSize];
		
		for (int i = 0; i < listSize; i++) {
			rt[i] = processList.get(i).getExecutionTime();
		}
		
		remain = listSize;
		for (count = 0, time = 0; remain != 0;) {
			if (rt[count] <= quantum && rt[count] > 0) {
				time += rt[count]; 
			    rt[count] = 0; 
			    flag = true; 
			}
			else if (rt[count]>0) {
				rt[count] -= quantum; 
			     time += quantum;
			}
			
			if ( rt[count] == 0 && flag) {
				remain--;
				waitTime += time - processList.get(count).getArrivalTime() - processList.get(count).getExecutionTime();
				returnTime += time - processList.get(count).getArrivalTime();
				responseTime += time - processList.get(count).getExecutionTime();
				flag = false;
			}
			
			if (count == listSize - 1) {
				count = 0;
			}
			else if(processList.get(count + 1).getArrivalTime() <= time) {
				count++;
			}
			else {
				count = 0;
			}
		}
		
		float averageWt = waitTime / listSize;
		float averageRt = returnTime / listSize;
		float averageRs = responseTime / listSize;
		
		System.out.printf("%.1f %.1f %.1f", averageRt, averageRs, averageWt);
	}
	
	public static void showAverageValues(ArrayList<Process> processList) {
		float averageWt = 0;
		float averageRt = 0;
		float averageRs = 0;
		
		for (Process proc : processList) {
			averageWt += proc.getWaitTime();
			averageRt += proc.getReturnTime();
			averageRs += proc.getResponseTime();
		}
		
		int numOfProcesses = processList.size();
		
		averageWt /= numOfProcesses;
		averageRt /= numOfProcesses;
		averageRs /= numOfProcesses;
		
		System.out.printf("%.1f %.1f %.1f", averageRt, averageRs, averageWt);
	}
	

	public static void main(String args[]) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Entre com o caminho do arquivo: ");
		String inputFile = scan.nextLine();
		scan.close();
			
		ArrayList<Process> processesfcfs = new ArrayList<Process>();
		ArrayList<Process> processessjf = new ArrayList<Process>();
		ArrayList<Process> processesrr = new ArrayList<Process>();
		BufferedReader bffReader = null;
		
		try {
			bffReader = new BufferedReader(new FileReader(inputFile));
			
			String currentLine;
			String[] tokens;
			int priority = 10;
			while ((currentLine = bffReader.readLine()) != null) {
				tokens = currentLine.split(" ");
				if (tokens.length < 2){
					System.err.println("Insuficient params. Two values needed!");
					System.exit(1);
				}
				processesfcfs.add(new Process(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]), priority));
				processessjf.add(new Process(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]), priority));
				processesrr.add(new Process(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]), priority));
				priority--;
			}
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		finally {
			try {
				if(bffReader != null)
					bffReader.close();
            }
			catch (IOException ex) {
                ex.printStackTrace();
            }
		}
		
		System.out.print("FCFS ");
		fcfs(processesfcfs);
		
		System.out.print("\nSJF ");
		sjf(processessjf);

		System.out.print("\nRR ");
		rr(processesrr, 2); // Quantum == 2
		
		return;
	}
}
